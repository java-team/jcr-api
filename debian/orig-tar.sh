#!/bin/sh 

set -e

package=`dpkg-parsechangelog | sed -n 's/^Source: //p'`
version=`dpkg-parsechangelog | grep '^Version:' | awk '{ print $2 }' | sed 's/-[^-]*$//'`
TAR=${package}_${version}.orig.tar.gz
DIR=${package}-${version}.orig
REPO="svn://svn.caucho.com/resin/tags/4.0.14/modules/jcr"

svn export $REPO $DIR
GZIP=--best tar --numeric --group 0 --owner 0 -c -v -z -f $TAR $DIR

rm -rf $DIR
